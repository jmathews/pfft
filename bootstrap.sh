#!/usr/bin/env bash

apt-get install -y language-pack-en python-dev python-pip gcc libxml2 libxml2-dev libxslt1-dev zlib1g-dev

cd /vagrant
pip install -r requirements.txt

