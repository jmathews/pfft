from django.contrib import admin
from models import Film, Screening, Cinema, Screen

admin.site.register(Film)
admin.site.register(Screening)
admin.site.register(Cinema)
admin.site.register(Screen)
