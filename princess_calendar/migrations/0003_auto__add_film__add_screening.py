# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Film'
        db.create_table(u'princess_calendar_film', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=2048)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=1024)),
        ))
        db.send_create_signal(u'princess_calendar', ['Film'])

        # Adding model 'Screening'
        db.create_table(u'princess_calendar_screening', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=2048)),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['princess_calendar.Film'])),
        ))
        db.send_create_signal(u'princess_calendar', ['Screening'])


    def backwards(self, orm):
        # Deleting model 'Film'
        db.delete_table(u'princess_calendar_film')

        # Deleting model 'Screening'
        db.delete_table(u'princess_calendar_screening')


    models = {
        u'princess_calendar.film': {
            'Meta': {'object_name': 'Film'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '2048'})
        },
        u'princess_calendar.screening': {
            'Meta': {'object_name': 'Screening'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Film']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '2048'})
        }
    }

    complete_apps = ['princess_calendar']