# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Screening.end_time'
        db.add_column(u'princess_calendar_screening', 'end_time',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now()),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Screening.end_time'
        db.delete_column(u'princess_calendar_screening', 'end_time')


    models = {
        u'princess_calendar.film': {
            'Meta': {'object_name': 'Film'},
            'duration': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        },
        u'princess_calendar.screening': {
            'Meta': {'object_name': 'Screening'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Film']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        }
    }

    complete_apps = ['princess_calendar']