# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Cinema.added_on'
        db.alter_column(u'princess_calendar_cinema', 'added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Screen.added_on'
        db.alter_column(u'princess_calendar_screen', 'added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Film.added_on'
        db.alter_column(u'princess_calendar_film', 'added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Screening.added_on'
        db.alter_column(u'princess_calendar_screening', 'added_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

    def backwards(self, orm):

        # Changing field 'Cinema.added_on'
        db.alter_column(u'princess_calendar_cinema', 'added_on', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Screen.added_on'
        db.alter_column(u'princess_calendar_screen', 'added_on', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Film.added_on'
        db.alter_column(u'princess_calendar_film', 'added_on', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Screening.added_on'
        db.alter_column(u'princess_calendar_screening', 'added_on', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        u'princess_calendar.cinema': {
            'Meta': {'object_name': 'Cinema'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'})
        },
        u'princess_calendar.film': {
            'Meta': {'object_name': 'Film'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'duration': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '1024'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        },
        u'princess_calendar.screen': {
            'Meta': {'object_name': 'Screen'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cinema': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Cinema']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '512', 'null': 'True'})
        },
        u'princess_calendar.screening': {
            'Meta': {'object_name': 'Screening'},
            'added_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 30, 0, 0)'}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Film']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'screen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['princess_calendar.Screen']", 'null': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 12, 30, 0, 0)'}),
            'url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '2048'})
        }
    }

    complete_apps = ['princess_calendar']