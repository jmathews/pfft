from django.http import HttpResponse
from princesscinemas import get_modified_calendar

def icalendar(request):
        response = HttpResponse(get_modified_calendar(), mimetype='text/calendar')
        response['Content-Disposition'] = 'attachment: filename=calendar.ics'
        return response